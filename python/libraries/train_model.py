import cv2  # 导入opencv库
import os  # 导入os库
import numpy as np  # 导入numpy库
from PIL import Image  # 导入PIL库Image模块




# 初始化人脸检测器和识别器
detector=cv2.CascadeClassifier(cv2.data.haarcascades+ 'haarcascade_frontalface_default.xml') # 加载人脸检测分类器
recognizer = cv2.face.LBPHFaceRecognizer_create()
def getfilename(filename):
    file_num = os.path.dirname(filename) 
    image_id = file_num.split("/")[-1]
    #print(image_id)
    return image_id
'''
遍历图片路径，导入图片和id，添加到list
'''

def get_images_and_labels(path):
    # 获取文件夹名字
    filename = os.listdir(path)
    face_samples = [] 
    # 创建id样本列表（空）
    ids = []  
    for name in filename:
        #print(fag)
        # 拼接图片路径,对应到具体的一张张图片名称,如/root/image/project14/new/50.1.jpg,存储到列表image_paths中
        image_paths = [os.path.join(path+name, f) for f in os.listdir(path+name)] 
        #print(image_paths)

        for image_path in image_paths: # 遍历图片路径
            # 打印每张图片的名称（带路径的图片名）
            #print(image_path) 
            # 将彩色图片转化为灰度图
            image = Image.open(image_path).convert('L')
            # 再将灰度图的格式转换成了Numpy数组
            image_np = np.array(image, 'uint8')
            
            # 检测array数组格式的人脸图片并将结果存储到faces中
            faces = detector.detectMultiScale(image_np) # 检测人脸
            # 将array数组格式的人脸图片裁剪出人脸部分后都存储到人脸样本列表中,再将图片的ID号都存储到id样本列表中
            for (x, y, w, h) in faces:
                #print("加载")
                face_samples.append(image_np[y:y + h, x:x + w])
                ids.append(int(name))
    
    return face_samples, ids # 返回人脸样本列表和id样本列表