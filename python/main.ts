
//% color="#8b92e8" iconWidth=50 iconHeight=40
namespace face_recognition{

    //% block="初始化摄像头直到成功 编号[CAMNUM]" blockType="command"
    //% CAMNUM.shadow="number" CAMNUM.defl="0"
    export function readcap(parameter: any, block: any) {
        let num=parameter.CAMNUM.code;
 

        Generator.addImport(`import time\nimport cv2\nimport numpy as np`)
        Generator.addCode(`def numberMap(x, in_min, in_max, out_min, out_max):
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min\n`)    
        Generator.addCode(`def drawChinese(text,x,y,size,r, g, b, a,img):
    font = ImageFont.truetype("HYQiHei_50S.ttf", size)
    img_pil = Image.fromarray(img)
    draw = ImageDraw.Draw(img_pil)
    draw.text((x,y), text, font=font, fill=(b, g, r, a))
    frame = np.array(img_pil)
    return frame\n`)    
        Generator.addCode(`cap = cv2.VideoCapture(${num})`)
        Generator.addCode(`cap.set(cv2.CAP_PROP_FRAME_WIDTH, 240)`)
        Generator.addCode(`cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 320)`)
        Generator.addCode(`cap.set(cv2.CAP_PROP_BUFFERSIZE, 1)`)
        Generator.addCode(`cv2.namedWindow('cvwindow',cv2.WND_PROP_FULLSCREEN)`)
        Generator.addCode(`cv2.setWindowProperty('cvwindow', cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)`)
        Generator.addCode(`pic_counta = 0`) 
        Generator.addCode(`pic_countb = 0`)
        Generator.addCode(`font = cv2.FONT_HERSHEY_SIMPLEX`)               
    Generator.addCode(`while not cap.isOpened():
    continue`)    
    }


    //% block="导入人脸检测模型" blockType="command"

    export function loadxml(parameter: any, block: any) {
              
        Generator.addCode(`detector=cv2.CascadeClassifier(cv2.data.haarcascades+ 'haarcascade_frontalface_default.xml')`)  
        Generator.addCode(`recognizer = cv2.face.LBPHFaceRecognizer_create()`) 
    }



 
    //% block="等待直到按下[BUTTON]保存[PIC]张人脸图片到文件夹 路径[PATH] 人脸ID编号[NAME]" blockType="command"
    //% PATH.shadow="string" PATH.defl="/root/face_recognition/picture"
    //% PIC.shadow="range" PIC.params.min=0    PIC.params.max=500    PIC.defl=50
    //% NAME.shadow="number" NAME.defl="0"
    //% BUTTON.shadow="dropdown" BUTTON.options="BUTTON"
    export function saveImage1(parameter: any, block: any) {
        let path=parameter.PATH.code;
        let name=parameter.NAME.code;
        let button=parameter.BUTTON.code;
        let pic=parameter.PIC.code;
        path=replaceQuotationMarks(path)
 

        Generator.addImport(`import datetime\nimport os\nimport time`)
        Generator.addCode(`

while not (cv2.waitKey(10)==ord('${button}')):
    time.sleep(0.1)

while not (pic_count${button} >=${pic}):
    cvimg_success, img_src = cap.read()
    if cvimg_success:
        cvimg_h, cvimg_w, cvimg_c = img_src.shape
        cvimg_w1 = cvimg_h*240//320
        cvimg_x1 = (cvimg_w-cvimg_w1)//2
        img_src = img_src[:, cvimg_x1:cvimg_x1+cvimg_w1]
        img_src = cv2.resize(img_src, (240, 320))
        gray = cv2.cvtColor(img_src, cv2.COLOR_BGR2GRAY)
        faces = detector.detectMultiScale(gray, 1.3, 5)
        cv2.imshow('cvwindow', img_src)
        img_dir_path ="/root/face_recognition/picture/" + str(${name})+"/"
        img_name_path=str(datetime.datetime.now().strftime('%Y%m%d_%H%M%S_%f'))+".jpg"
        img_save_path=img_dir_path+img_name_path
        try:
            if not os.path.exists(img_dir_path):
                print("The folder does not exist,created automatically")
                os.system("mkdir -p ${path}/" + str(${name}))
                time.sleep(0.1)
        except IOError:
            print("IOError,created automatically")
            break
        for (x, y, w, h) in faces: 
            cv2.rectangle(img_src, (x, y), (x + w, y + h), (0, 255, 0), 2)  
            if pic_count${button} <= ${pic}: 
                cv2.putText(img_src, 'shooting', (10, 50), font, 0.6, (0, 255, 0), 2) 
                cv2.imwrite(img_save_path,img_src)
                pic_count${button}=pic_count${button}+1
                print("save picture path:",img_save_path)
                print("save"+ str(${name})+"-picture count:"+str(pic_count${button}))
                cv2.putText(img_src, str(${name}), (x, y+h), font, 0.6, (0, 0, 255), 2)
                time.sleep(0.05)
            else: 
                cv2.putText(img_src, 'Done,Please quit', (10, 50), font, 0.6, (0, 255, 0), 2) 
                time.sleep(2) 

        cv2.imshow('cvwindow', img_src)
        cv2.waitKey(5)
cap.release()
cv2.destroyAllWindows() 

`)
 
    }
    
    //% block="---"
    export function noteSep2() {

    }

    //% block="导入人脸图片数据集路径[PATH]训练人脸识别模型" blockType="command"
    //% PATH.shadow="string" PATH.defl="/root/face_recognition/picture/"
    export function tarinmodel(parameter: any, block: any) {
        let path=parameter.PATH.code;
 

        path=replaceQuotationMarks(path)

        Generator.addCode(`detector=cv2.CascadeClassifier(cv2.data.haarcascades+ 'haarcascade_frontalface_default.xml')`)  
        Generator.addCode(`recognizer = cv2.face.LBPHFaceRecognizer_create()`) 
        Generator.addImport(`import train_model\nimport cv2\nimport numpy as np\n`)
        Generator.addCode(`faces, Ids = train_model.get_images_and_labels('${path}')`)
        Generator.addCode(`recognizer.train(faces, np.array(Ids))`) 
        
    }    
    //% block="保存训练后的模型文件 路径[PATH]" blockType="command"
    //% PATH.shadow="string" PATH.defl="/root/face_recognition/model/model.yml"
    export function savemodel(parameter: any, block: any) {
        let path=parameter.PATH.code;

        path=replaceQuotationMarks(path)
        Generator.addImport(`import os\nimport time`)
        Generator.addCode(`img_dir_path ="${path}"
img_dir_path = os.path.abspath(os.path.dirname(img_dir_path))
print(img_dir_path)
try:
    if not os.path.exists(img_dir_path):
        print("The folder does not exist,created automatically")
        os.mkdir(img_dir_path)
        time.sleep(0.1)
except IOError:
    print("IOError,created automatically")`)
        Generator.addCode(`recognizer.save("${path}")`)
        
 
    } 
    //% block="---"
    export function noteSep3() {

    } 
    //% block="导入训练后的模型文件 路径[PATH]" blockType="command"
    //% PATH.shadow="string" PATH.defl="/root/face_recognition/model/model.yml"
    export function shurumodel(parameter: any, block: any) {
        let path=parameter.PATH.code;
        
        Generator.addCode(`detector=cv2.CascadeClassifier(cv2.data.haarcascades+ 'haarcascade_frontalface_default.xml')`)  
        Generator.addCode(`recognizer = cv2.face.LBPHFaceRecognizer_create()`) 
        Generator.addCode(`recognizer.read(${path})`)
        Generator.addCode(`global img_id, confidence`)
        Generator.addCode(`img_id, confidence = 0,0`)
 
    }  
    //% block="打开摄像头画面进行读取一帧" blockType="command"
    export function readcapcapture(parameter: any, block: any) {

        Generator.addCode(`
cvimg_success, img_src = cap.read()
if cvimg_success:
    cvimg_h, cvimg_w, cvimg_c = img_src.shape
    cvimg_w1 = cvimg_h*240//320
    cvimg_x1 = (cvimg_w-cvimg_w1)//2
    img_src = img_src[:, cvimg_x1:cvimg_x1+cvimg_w1]
    img_src = cv2.resize(img_src, (240, 320))
    gray = cv2.cvtColor(img_src, cv2.COLOR_BGR2GRAY)  
    faces = detector.detectMultiScale(gray, 1.3, 5)  
    cv2.imshow('cvwindow', img_src)
`)


    }
     //% block="判断是否有人脸?" blockType="boolean"
     export function faceass(parameter: any, block: any) {
        Generator.addCode(`len(faces)>0`)
  
    
}    
    //% block="开始进行人脸识别" blockType="command"
    export function faceas(parameter: any, block: any) {
        Generator.addCode(`img_id, confidence = 0,0`)
        Generator.addCode(`for (x, y, cvimg_w, cvimg_h) in faces:
    cv2.rectangle(img_src, (x - 10, y - 10), (x + cvimg_w + 10, y + cvimg_h + 10), (0, 255, 0), 2)
    img_array = gray[y:y + cvimg_h, x:x + cvimg_w]
    img_id, confidence = recognizer.predict(img_array)
    confidence   = (numberMap(confidence, 150, 0,0,100))
    if confidence ==100:
        confidence = 0
`)
    
} 
    //% block="在摄像头画面上显示文字[TEXT]大小[SIZE] 颜色R[R]G[G]B[B] 坐标X[X]Y[Y]" blockType="command"
    //% TEXT.shadow="string" TEXT.defl="你好"
    //% SIZE.shadow="number"   SIZE.defl="25"
    //% R.shadow="range"   R.params.min=0    R.params.max=255    R.defl=50
    //% G.shadow="range"   G.params.min=0    G.params.max=255    G.defl=200
    //% B.shadow="range"   B.params.min=0    B.params.max=255    B.defl=0
    //% X.shadow="number"   X.defl="10"
    //% Y.shadow="number"   Y.defl="20"
    export function drawText(parameter: any, block: any) {
        let txt=parameter.TEXT.code;
        let size=parameter.SIZE.code;
        let r=parameter.R.code;
        let g=parameter.G.code;
        let b=parameter.B.code;
        let x=parameter.X.code;
        let y=parameter.Y.code;

        //Generator.addCode(`cv2.putText(img_src, str(${txt}), (${x}, ${y}), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (${b}, ${g}, ${r}), 2)`)   
         
        Generator.addCode(`img_src = drawChinese(text=str(${txt}),x=${x}, y=${y},size=${size},r= ${r},g=${g},b=0,a=${b},img=img_src)`)    
    }
    //% block="将人脸识别结果显示到屏幕上" blockType="command"
    export function show(parameter: any, block: any) {
        Generator.addImport(`from PIL import ImageFont, ImageDraw, Image`)
        Generator.addCode(`cv2.imshow('cvwindow', img_src)
cv2.waitKey(5)`)
    }
    //% block="预测加载的图像 返回识别结果索引" blockType="reporter"
    export function predictid(parameter: any, block: any) {
        Generator.addCode(`img_id`)

    } 
    //% block="预测加载的图像 返回识别结果置信度" blockType="reporter"
    export function predictPercentage(parameter: any, block: any) {
        Generator.addCode(`confidence`)

    } 
    function replaceQuotationMarks(str:string){
        str=str.replace(/"/g, ""); //去除所有引号
        return str
}


}


