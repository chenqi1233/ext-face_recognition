# 人脸识别


![](./python/_images/featured.png)


# 常见问题
## 1、不连接行空板能否使用这个库
不可以，这个库里面有很多路径专为行空板设计，如果不连接行空板运行可能会出错。

## 2、报错：AttributeError: module 'cv2' has no attribute 'face'
运行程序报“AttributeError: module 'cv2' has no attribute 'face'”错误，说明行空板中缺少依赖库。
![](./python/_images/cv2error.png)
### 解决方法：

#### 方法一：离线更新缺少的依赖库
- 首先[点击下载](https://gitee.com/chenqi1233/ext-face_recognition/releases/download/0.0.2/%E7%A6%BB%E7%BA%BF%E7%BB%99%E8%A1%8C%E7%A9%BA%E6%9D%BF%E5%AE%89%E8%A3%85opencv_contrib_python%E5%BA%93.mp)**离线给行空板安装opencv_contrib_python库.mp**文件，使用Mind+打开此项目文件。注意：**行空板系统0.3.4以上**（如下图1）
- Mind+远程连接行空板，点击运行，等待终端提示”Successfully installed opencv-contrib-python-4.5.5.64“即为依耐库安装成功，之后行空板会自动重启。（如下图2）
- 安装完成之后，运行之前报错的程序，不会报这个错误了。（如下图3）

![](./python/_images/安装库1.png)
![](./python/_images/安装库完成.png)
![](./python/_images/正常.png)



#### 方法二：升级行空板系统镜像到V0.3.5及以上版本
行空板从0.3.5版本开始内置了这个库，所以刷完系统之后这个依赖库就自动有了，缺点是系统镜像文件比较大可能需要下载较长时间。[升级镜像教程链接](https://www.unihiker.com.cn/wiki/burner)


# 积木

![](./python/_images/block.png)

# 程序实例

步骤1
![](./python/_images/explame1.png)

步骤2

![](./python/_images/explame2.png)

步骤3

![](./python/_images/explame3.png)





# 支持列表

|主板型号|实时模式|ArduinoC|MicroPython|python|备注|
|-----|-----|-----|-----|:-----:|-----|
|uno|||||||
|micro:bit|||||||
|mpython|||||||
|arduinonano|||||||
|leonardo|||||||
|mega2560|||||||
|行空板||||√|||

# 更新日志

V0.0.1 基础功能完成

V0.0.2 更新积木名称和代码优化

V0.0.3 更新文字显示颜色问题

V0.0.4 更新文字可显示中文

V0.0.5 更新识别人脸框大小

V0.0.6 调整依赖库

V0.0.7 更新人脸ID可填入变量

V0.0.8 更新图像判断代码

V0.0.9 文件生成代码错误